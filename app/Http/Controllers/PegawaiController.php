<?php

namespace App\Http\Controllers;

use App\Models\pegawai;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    public function index(Request $request)
    {
       $data = pegawai::get();
        if ($request->ajax()) {
            return datatables()->of($data)
            ->addColumn('aksi', function ($data)
            {
                $button = "<button class='detail btn btn-success'  id='".$data->id."'>
                            <i class='fas fa-eye'></i>
                            </button>";
                $button .= " <button class='edit btn btn-primary' id='".$data->id."'>
                            <i class='fas fa-edit'></i>
                            </button>";
                $button .= " <button class='hapus btn btn-danger' id='".$data->id."'>
                            <i class='fas fa-trash'></i>
                            </button>";
                return $button;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }
        return view('admin.data');
    }

    public function store(Request $request)
    {
        $data = new pegawai();
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->telepon = $request->telepon;
        $data->tanggal_lahir = $request->tanggal_lahir;
        $data->alamat = $request->alamat;
        $data->jabatan = $request->jabatan;
        $simpan = $data->save();
        if ($simpan) {
            return response()->json(['data' => $data, 'text' => 'Data berhasil disimpan'], 200);
        } else {
            return response()->json(['data' => $data, 'text' => 'Gagal menyimpan data'], 400);
        }
    }

    public function show($id)
    {
        $pegawai = Pegawai::find($id);
        return response()->json(['data' => $pegawai], 200);
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $data = pegawai::find($id);
        return response()->json(['data' => $data]);
    }

    public function update(Request $request)
    {
        // dd($request->all());
        $id = $request->id;
        $datas = [
            'nama' => $request->nama,
            'email' => $request->email,
            'telepon' => $request->telepon,
            'tanggal_lahir' => $request->tanggal_lahir,
            'alamat' => $request->alamat,
            'jabatan' => $request->jabatan,
        ];
        $data = pegawai::find($id);
        $simpan = $data->update($datas);
        if ($simpan) {
            return response()->json(['text' => 'Data berhasil diubah'], 200);
        } else {
            return response()->json(['text' => 'Data gagal diubah'], 400);
        }
    }

    public function hapus(Request $request)
    {
        $id = $request->id;
        $data = pegawai::find($id);
        $data->delete();
        return response()->json(['text' => 'Data berhasil dihapus'], 200);
    }

}
