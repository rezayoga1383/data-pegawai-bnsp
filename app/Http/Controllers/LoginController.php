<?php

namespace App\Http\Controllers;

use App\Models\pegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function autentikasi(Request $request)
    {
        // dd($request->all());
        Session::flash('email', $request->email);
        Session::flash('password', $request->password);

        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ], [
            'email.required' => 'email wajib diisi',
            'password.required' => 'password wajib diisi',
        ]);

        $infologin = [
            'email' => $request->email,
            'password' => $request->password
        ];

        // dd($infologin);
        // $pengguna = Pengguna::all();
        // dd($pengguna);
        // Cek kredensial pengguna
        if (Auth::attempt($infologin)){
            if (Auth::user()->role == "Admin"){
                return redirect('/admin/dashboard');
            }else{
                return redirect('/user/dashboard');
            }
        }else{
            return redirect()->route('login')->with('notvalid','email dan password yang dimasukkan tidak sesuai');
        } 
    }

    public function admin()
    {
        $totalPegawai = pegawai::count();
        $kasirCount = pegawai::where('jabatan', 'kasir')->count();
        $manajerCount = pegawai::where('jabatan', 'manajer')->count();
        $adminCount = pegawai::where('jabatan', 'admin')->count();

        return view('admin.dashboard', compact('totalPegawai', 'kasirCount', 'manajerCount', 'adminCount'));
        // return view('admin.dashboard');
    }
    
    public function user()
    {
        return view('admin.dashboard-user');
    }
}
