<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="{{  asset('template/dist/assets/static/js/initTheme.js')}}"></script>
<script src="{{  asset('template/dist/assets/static/js/components/dark.js')}}"></script>
<script src="{{  asset('template/dist/assets/extensions/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{  asset('template/dist/assets/compiled/js/app.js')}}"></script>
<!-- Need: Apexcharts -->
{{-- <script src="{{  asset('template/dist/assets/extensions/apexcharts/apexcharts.min.js')}}"></script> --}}
{{-- <script src="{{  asset('template/dist/assets/static/js/pages/dashboard.js')}}"></script> --}}


{{-- <script src="{{  asset('template/dist/assets/extensions/jquery/jquery.min.js')}}"></script>
<script src="{{  asset('template/dist/assets/extensions/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{  asset('template/dist/assets/extensions/datatables.net-bs5/js/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{  asset('template/dist/assets/static/js/pages/datatables.js')}}"></script> --}}