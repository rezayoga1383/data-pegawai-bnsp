<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    @include('admin.layout.style')
</head>

<body>
    
    <div id="app">
        @include('admin.layout.sidebar')
    </div>

        <div id="main">
            <section class="section">
                @yield('content')
            </section>
        </div>
            
        <footer>
            <div class="footer clearfix mb-0 text-muted">
                <div class="float-start">
                    <p>2024 &copy; Indimirit</p>
                </div>
                <div class="float-end">
                    <p>Crafted with <span class="text-danger"><i class="bi bi-heart-fill icon-mid"></i></span>
                        by <a href="https://saugi.me">rejaayp</a></p>
                </div>
            </div>
        </footer>

        </div>
    </div>
    
    @include('admin.layout.script')

</body>

</html>
@stack('script')