@extends('admin.layout.main')

@section('title', 'Data Pegawai - Admin')

@section('content')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Data Pegawai</h3>
                <p class="text-subtitle text-muted">Administrator melakukan pengelolaan data pegawai.</p>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard-admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data Pegawai</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- Minimal jQuery Datatable start -->
    <section class="section">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5 class="card-title mb-0">Data Pegawai</h5>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" id="tambah">
                    + Tambah Data
                </button>
            </div>
            <div class="card-body">
                <div class="table-responsive datatable-minimal">
                    <table class="table" id="table1">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                {{-- <th>Email</th> --}}
                                <th>Telepon</th>
                                {{-- <th>Tanggal Lahir</th> --}}
                                {{-- <th>Alamat</th> --}}
                                <th>Jabatan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- Minimal jQuery Datatable end -->
</div>

  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="title">Form Data Pegawai</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            @csrf
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Nama</label>
                <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama Pegawai" name="nama">
                <input type="hidden" id="id" name="id">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Masukkan Email Pegawai" name="email">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">No Telepon</label>
                <input type="text" class="form-control" id="telepon" placeholder="Masukkan No Telepon Pegawai" name="telepon">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Tanggal Lahir</label>
                <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Alamat</label>
                <textarea class="form-control" name="alamat" id="alamat" cols="30" rows="5" placeholder="Masukkan Alamat Pegawai"></textarea>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Jabatan</label>
                <input type="text" class="form-control" id="jabatan" placeholder="Masukkan Jabatan Pegawai" name="jabatan">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="tutup" class="btn btn-secondary" data-bs-dismiss="modal">Kembali</button>
          <button type="button" id="simpan" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </div>
  </div>

   <!-- Detail Modal -->
<div class="modal fade" id="detailModal" tabindex="-1" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailModalLabel">Detail Pegawai</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p><strong>Nama: </strong><span id="detail-nama"></span></p>
                <p><strong>Email: </strong><span id="detail-email"></span></p>
                <p><strong>Telepon: </strong><span id="detail-telepon"></span></p>
                <p><strong>Tanggal Lahir: </strong><span id="detail-tanggal_lahir"></span></p>
                <p><strong>Alamat: </strong><span id="detail-alamat"></span></p>
                <p><strong>Jabatan: </strong><span id="detail-jabatan"></span></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@push('script')
<script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/2.0.8/js/dataTables.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

{{-- read data pegawai --}}
<script>
    $(document).ready(function (){
        isi()
    })

    function isi() {
        $('#table1').DataTable({
            serverside : true,
            responsive : true,
            ajax : {
                url : "{{ route('data-pegawai') }}",
                dataSrc: function (json) {
                        return json.data;
                    }
            },
            columns: 
                [
                    {
                        "data" : null, "sortable" : false,
                        render : function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1
                        }
                    },
                    {data: 'nama', name: 'nama'},
                    // {data: 'email', name: 'email'},
                    {data: 'telepon', name: 'telepon'},
                    // {data: 'tanggal_lahir', name: 'tanggal_lahir'},
                    // {data: 'alamat', name: 'alamat'},
                    {data: 'jabatan', name: 'jabatan'},
                    {data: 'aksi', name: 'aksi'},
                ]
        })
    }
</script>

{{-- create data pegawai --}}
<script>
    $('#simpan').on('click', function () {
        if ($(this).text() === 'Simpan Edit') {
            edit()
        }else{
            tambah()
        }
    });

    // fungsi create data pegawai
    function tambah() {
        $.ajax({
                url  : "{{ route('data-store') }}",
                type : "post",
                data : {
                    nama : $('#nama').val(),
                    email : $('#email').val(),
                    telepon : $('#telepon').val(),
                    tanggal_lahir : $('#tanggal_lahir').val(),
                    alamat : $('#alamat').val(),
                    jabatan : $('#jabatan').val(),
                    "_token" : "{{ csrf_token()}}"
                },
                success : function (res) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Berhasil',
                        text: res.text,
                    });
                    $('#tutup').click();
                    $('#table1').DataTable().ajax.reload();
                    $('#nama').val(null);
                    $('#email').val(null);
                    $('#telepon').val(null);
                    $('#tanggal_lahir').val(null);
                    $('#alamat').val(null);
                    $('#jabatan').val(null);
                },
                error : function (xhr) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Gagal',
                        text: xhr.responseJSON.text,
                    });
                }
            });
    }

    // edit data pegawai
    $(document).on('click','.edit', function() {
        let id = $(this).attr('id')
        $('#tambah').click()
        $('#simpan').text('Simpan Edit')

        $.ajax({
            url : "{{ route('data-edit') }}",
            type : 'post',
            data : {
                id : id,
                _token : "{{ csrf_token() }}"
            },
            success : function (res) {
                $('#id').val(res.data.id);
                $('#nama').val(res.data.nama);
                $('#email').val(res.data.email);
                $('#telepon').val(res.data.telepon);
                $('#tanggal_lahir').val(res.data.tanggal_lahir);
                $('#alamat').val(res.data.alamat);
                $('#jabatan').val(res.data.jabatan);
            }
        })
    })

    function edit() {
        $.ajax({
                url  : "{{ route('data-update') }}",
                type : "post",
                data : {
                    id : $('#id').val(),
                    nama : $('#nama').val(),
                    email : $('#email').val(),
                    telepon : $('#telepon').val(),
                    tanggal_lahir : $('#tanggal_lahir').val(),
                    alamat : $('#alamat').val(),
                    jabatan : $('#jabatan').val(),
                    "_token" : "{{ csrf_token()}}"
                },
                success : function (res) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Berhasil',
                        text: res.text,
                    });
                    $('#tutup').click();
                    $('#table1').DataTable().ajax.reload();
                    $('#nama').val(null);
                    $('#email').val(null);
                    $('#telepon').val(null);
                    $('#tanggal_lahir').val(null);
                    $('#alamat').val(null);
                    $('#jabatan').val(null);
                    $('#simpan').text('Simpan')
                },
                error : function (xhr) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Gagal',
                        text: xhr.responseJSON.text,
                    });
                }
            });
    }

    $(document).on('click','.hapus', function() {
        let id = $(this).attr('id')
        $.ajax({
            url : "{{ route('data-hapus') }}",
            type : 'post',
            data : {
                id: id,
                "_token" : "{{ csrf_token() }}"
            },
            success: function (params) {
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil',
                    text: params.text,
                });
                $('#table1').DataTable().ajax.reload();
            }
        })
    })


    $(document).on('click', '.detail', function() {
        let id = $(this).attr('id');
        $.ajax({
            url: "{{ route('detail-pegawai', '') }}/" + id,
            type: 'GET',
            success: function(res) {
                $('#detailModal').modal('show');
                $('#detail-nama').text(res.data.nama);
                $('#detail-email').text(res.data.email);
                $('#detail-telepon').text(res.data.telepon);
                $('#detail-tanggal_lahir').text(res.data.tanggal_lahir);
                $('#detail-alamat').text(res.data.alamat);
                $('#detail-jabatan').text(res.data.jabatan);
            }
        });
    });
</script>

@endpush
@endsection
  