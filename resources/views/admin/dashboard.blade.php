@extends('admin.layout.main')

@section('title', 'Dashboard - Admin')

@section('content')
<header class="mb-3">
<a href="#" class="burger-btn d-block d-xl-none">
    <i class="bi bi-justify fs-3"></i>
</a>
</header>

<div class="page-heading">
<h3>Dashboard Admin</h3>
</div> 
<div class="page-content"> 
<section class="row">
<div class="col-12">
    <div class="row">
        <div class="col-6 col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body px-4 py-4-5">
                    <div class="row">
                        <div class="col-md-4 col-lg-12 col-xl-12 col-xxl-5 d-flex justify-content-start">
                            <div class="stats-icon purple mb-2">
                                <i class="fas fa-users"></i> <!-- Ubah ikon menjadi users -->
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-12 col-xl-12 col-xxl-7">
                            <h6 class="text-muted font-semibold">Pegawai</h6>
                            <h6 class="font-extrabold mb-0">{{ $totalPegawai }}</h6>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <!-- Sisipkan kartu-kartu lainnya di sini -->
        <div class="col-6 col-lg-3 col-md-6">
            <div class="card"> 
                <div class="card-body px-4 py-4-5">
                    <div class="row">
                        <div class="col-md-4 col-lg-12 col-xl-12 col-xxl-5 d-flex justify-content-start">
                            <div class="stats-icon blue mb-2">
                                <i class="fas fa-cash-register"></i> <!-- Mengubah ikon menjadi cash-register -->
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-12 col-xl-12 col-xxl-7">
                            <h6 class="text-muted font-semibold">Kasir</h6>
                            <h6 class="font-extrabold mb-0">{{ $kasirCount }}</h6>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body px-4 py-4-5">
                    <div class="row">
                        <div class="col-md-4 col-lg-12 col-xl-12 col-xxl-5 d-flex justify-content-start">
                            <div class="stats-icon green mb-2">
                                <i class="fas fa-user-tie"></i> <!-- Mengubah ikon menjadi user-tie -->
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-12 col-xl-12 col-xxl-7">
                            <h6 class="text-muted font-semibold">Manajer</h6>
                            <h6 class="font-extrabold mb-0">{{ $manajerCount }}</h6>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body px-4 py-4-5">
                    <div class="row">
                        <div class="col-md-4 col-lg-12 col-xl-12 col-xxl-5 d-flex justify-content-start">
                            <div class="stats-icon red mb-2">
                                <i class="fas fa-warehouse"></i> <!-- Mengubah ikon menjadi warehouse -->
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-12 col-xl-12 col-xxl-7">
                            <h6 class="text-muted font-semibold">Admin Gudang</h6>
                            <h6 class="font-extrabold mb-0">{{ $adminCount }}</h6>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
@endsection