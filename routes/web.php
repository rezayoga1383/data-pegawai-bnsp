<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\PegawaiController;
use App\Http\Controllers\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', [LoginController::class, 'login'])->name('login');
Route::get('/register', [RegisterController::class, 'index'])->name('register');

// register
Route::post('/daftar', [RegisterController::class, 'daftar'])->name('daftar');

// login
Route::post('/autentikasi', [LoginController::class, 'autentikasi'])->name('autentikasi');

// logout
Route::post('/logout', [LogoutController::class, 'logout'])->name('logout');


// dashboard admin
Route::get('/admin/dashboard',[LoginController::class, 'admin'])->name('dashboard-admin');
Route::get('/user/dashboard',[LoginController::class, 'user'])->name('dashboard-user');

// data pegawai
Route::get('/pegawai', function () {
    return view('admin.data');
});

Route::get('data-index',[PegawaiController::class, 'index'])->name('data-pegawai');
Route::post('data-create',[PegawaiController::class, 'store'])->name('data-store');
Route::post('data-edit', [PegawaiController::class, 'edit'])->name('data-edit');
Route::post('data-update', [PegawaiController::class, 'update'])->name('data-update');
Route::post('data-hapus', [PegawaiController::class, 'hapus'])->name('data-hapus');
Route::get('/data/detail/{id}', [PegawaiController::class, 'show'])->name('detail-pegawai');



// Route::get('/register', function () {
//     return view('register');
// });
// Route::get('/dashboard', function () {
//     return view('admin.dashboard');
// });
