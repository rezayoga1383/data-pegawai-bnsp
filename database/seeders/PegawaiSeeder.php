<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        $data = [];
        
        for ($i = 1; $i <= 10; $i++) {
            $data[] = [
                'nama' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'telepon' => $faker->phoneNumber,
                'tanggal_lahir' => $faker->date,
                'alamat' => $faker->address,
                'jabatan' => $faker->jobTitle,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }
        
        DB::table('pegawais')->insert($data);
    }
}
