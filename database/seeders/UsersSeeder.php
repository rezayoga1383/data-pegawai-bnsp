<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'nama' => 'Guru Gembul',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
            'role' => 'Admin',
        ]);
        User::create([
            'nama' => 'Reza',
            'email' => 'reza@gmail.com',
            'password' => bcrypt('reza'),
            'role' => 'Pengguna',
        ]);
    }
}
